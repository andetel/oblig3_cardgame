package idatx2001.oblig3.cardgame;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.util.function.ToDoubleBiFunction;

public class Controller {

    @FXML
    private ImageView img1;
    @FXML
    private ImageView img2;
    @FXML
    private ImageView img3;
    @FXML
    private ImageView img4;
    @FXML
    private ImageView img5;

    @FXML
    private TextField flush;
    @FXML
    private TextField cardsOfHeart;
    @FXML
    private TextField queenOfSpades;
    @FXML
    private TextField sumOfFaces;

    private DeckOfCards deckOfCards = new DeckOfCards();

    /**
     * Method that deals a new hand of cards
     */
    public void dealHand() {
        flush.setText("");
        cardsOfHeart.setText("");
        queenOfSpades.setText("");
        sumOfFaces.setText("");

        ImageView[] images = {img1, img2, img3, img4, img5};

        CardHand cardHand = this.deckOfCards.dealHand(5);

        for (int i = 0; i < cardHand.getCards().size(); i++) {
            images[i].setImage(new Image(Game.class.getResourceAsStream("/img/" + cardHand.getCards().get(i).getAsString() + ".png")));
        }
    }

    /**
     * Method that sums the faces of the cards on hand
     */
    public void sumOfFaces() {
        sumOfFaces.setText(Integer.toString(deckOfCards.getCardHand().getSumOfFaces()));
    }

    /**
     * Method that checks for the queen of spades on hand
     */
    public void queenOfSpades() {
        if (deckOfCards.getCardHand().noQueenOfSpades()) {
            queenOfSpades.setText("No");
        } else {
            queenOfSpades.setText("Yes");
        }
    }

    /**
     * Method that prints out all cards of heart on hand
     */
    public void cardsOfHeart() {
        if (deckOfCards.getCardHand().getHearts().isEmpty()) {
            cardsOfHeart.setText("No hearts");
        } else {
            StringBuilder hearts = new StringBuilder();

            for (PlayingCard playingCard: deckOfCards.getCardHand().getHearts()) {
                if (deckOfCards.getCardHand().getHearts().indexOf(playingCard) == deckOfCards.getCardHand().getHearts().size() - 1) {
                    hearts.append(playingCard.getAsString());
                } else {
                    hearts.append(playingCard.getAsString());
                    hearts.append(" ");
                }

            }

            cardsOfHeart.setText(hearts.toString());
        }
    }

    /**
     * Method that checks for flush on hand
     */
    public void flush() {
        if (deckOfCards.getCardHand().checkFlush()) {
            flush.setText("Yes");
        } else {
            flush.setText("No");
        }
    }

    /**
     * Method that runs sumOfFaces, queenOfSpades, cardsOfHeart and flush
     */
    public void checkHand() {
        sumOfFaces();
        queenOfSpades();
        cardsOfHeart();
        flush();
    }
}