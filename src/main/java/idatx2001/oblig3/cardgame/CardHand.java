package idatx2001.oblig3.cardgame;

import java.util.*;
import java.util.stream.Collectors;

public class CardHand {
    private ArrayList<PlayingCard> cards = new ArrayList<PlayingCard>();

    /**
     * Adds a playingCard to the CardHand
     *
     * @param playingCard
     */
    public void addCard(PlayingCard playingCard) {
        this.cards.add(playingCard);
    }

    /**
     * Returns an ArrayList of all the playingcards on hand
     *
     * @return ArrayList with playingcrads
     */
    public ArrayList<PlayingCard> getCards() {
        ArrayList<PlayingCard> cpCards = new ArrayList<PlayingCard>();

        for (PlayingCard playingCard: this.cards) {
            cpCards.add(new PlayingCard(playingCard.getSuit(), playingCard.getFace()));
        }

        return cpCards;
    }

    /**
     * Method that counts the sum of faces of all the cards on hand
     *
     * @return int
     */
    public int getSumOfFaces() {
        return this.cards.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    /**
     * Returns false if the cardhand contains the queen of spades, and true if it doesn't contain the queen of spades
     *
     * @return true if no queen of spades, and false otherwise
     */
    public boolean noQueenOfSpades() {
        return this.cards.stream()
                .filter(c -> c.getAsString()
                        .equals("S12"))
                .map(p -> Boolean.TRUE)
                .findAny()
                .isEmpty();
    }

    /**
     * Returns an ArrayList of all the cards of heart on hand
     *
     * @return ArrayList of cards of heart
     */
    public ArrayList<PlayingCard> getHearts() {
        return this.cards.stream()
                .filter(c -> Character.toString(c.getSuit()).equals("H"))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Returns true if the cards on hand creates a flush, otherwise returns false
     * @return true if flush, otherwise false
     */
    public boolean checkFlush() {
        HashSet<Character> pc = new HashSet<>();

        List<PlayingCard> lst = this.cards.stream()
                .filter(c -> !pc.add(c.getSuit()))
                .collect(Collectors.toList());

        if (lst.size() == 4) {
            return true;
        } else {
            return false;
        }
    }
}
