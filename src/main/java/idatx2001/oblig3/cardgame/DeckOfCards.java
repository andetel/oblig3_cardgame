package idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

public class DeckOfCards {
    private final int NUMBER_OF_CARDS_PER_SUIT = 13;
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private HashSet<PlayingCard> cardDeck;
    private CardHand cardHand;

    /**
     * Method that creates a new DeckOfCards object
     */
    public DeckOfCards() {
        this.cardDeck = new HashSet<PlayingCard>();

        for (int i = 0; i < this.suit.length; i++) {
            for (int j = 1; j <= NUMBER_OF_CARDS_PER_SUIT; j++ ) {
                this.cardDeck.add(new PlayingCard(this.suit[i], j));
            }
        }
    }

    /**
     * Method that returns all the cards in the deck as an ArrayList with PlayCard objects.
     * @return ArrayList with PlayCard objects
     */
    public ArrayList<PlayingCard> getCardDeck() {
        ArrayList<PlayingCard> cpCardDeck = new ArrayList<PlayingCard>();

        for (PlayingCard playingCard: this.cardDeck) {
            cpCardDeck.add(new PlayingCard(playingCard.getSuit(), playingCard.getFace()));
        }

        return cpCardDeck;
    }

    /**
     * Returns the card hand that has been created by the dealHand method
     *
     * @return CardHand object
     */
    public CardHand getCardHand() {
        return this.cardHand;
    }

    /**
     * Method that takes a number and returns an ArrayList of n random cards from the deck.
     * @param n number of random cards to return
     * @return CardHand object
     * @throws IllegalArgumentException if n is not between 1 and 52
     */
    public CardHand dealHand(int n) throws IllegalArgumentException{
        if (n < 1 || n > 52) {
            throw new IllegalArgumentException("The number of random cards must be between 1 and 52.");
        }

        CardHand randomCards = new CardHand();

        Random random = new Random();

        // To prevent same card to be chosen twice or more
        HashSet<Integer> usedIndexes = new HashSet<Integer>();

        while (randomCards.getCards().size() != n) {
            int randomNumber = random.nextInt(52);

            // checks if index is previously used
            if (!usedIndexes.contains(randomNumber)) {
                usedIndexes.add(randomNumber);
                randomCards.addCard(this.getCardDeck().get(randomNumber));

            }
        }

        this.cardHand = randomCards;

        return randomCards;
    }
}
