import idatx2001.oblig3.cardgame.DeckOfCards;
import idatx2001.oblig3.cardgame.PlayingCard;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

public class CardHandTest {

    @Test
    public void checkThatACardHandIsCreatedFromDealHandMethod() {
        DeckOfCards deckOfCards = new DeckOfCards();

        deckOfCards.dealHand(5);

        Assertions.assertNotNull(deckOfCards.getCardHand());
    }

    @Test
    public void checkIfTheSumOfFacesIsCorrect() {
        DeckOfCards deckOfCards = new DeckOfCards();

        deckOfCards.dealHand(5);

        int sum = 0;

        for (int i = 0; i < deckOfCards.getCardHand().getCards().size(); i++) {
            sum += deckOfCards.getCardHand().getCards().get(i).getFace();
        }

        Assertions.assertEquals(deckOfCards.getCardHand().getSumOfFaces(), sum);
    }

    @Test
    public void checkIfTheNoQueenOfSpadesIsCorrect() {
        DeckOfCards deckOfCards = new DeckOfCards();

        deckOfCards.dealHand(5);

        boolean isNotQueenOfSpades = true;

        for (PlayingCard playingCard: deckOfCards.getCardHand().getCards()) {
            if (playingCard.getAsString().equals("S12")) {
                isNotQueenOfSpades = false;
                break;
            }
        }

        Assertions.assertEquals(deckOfCards.getCardHand().noQueenOfSpades(), isNotQueenOfSpades);
    }

    @Test
    public void checkIfTheCheckFlushMethodIsCorrect() {
        DeckOfCards deckOfCards = new DeckOfCards();

        deckOfCards.dealHand(5);

        boolean flush = true;

        ArrayList<Character> suits = new ArrayList<Character>();

        for (int i = 0; i < deckOfCards.getCardHand().getCards().size(); i++) {
            if (i == 0) {
                suits.add(deckOfCards.getCardHand().getCards().get(i).getSuit());
            } else if (i < deckOfCards.getCardHand().getCards().size()) {
                if (suits.get(0) != deckOfCards.getCardHand().getCards().get(i).getSuit()) {
                    flush = false;
                    break;
                }
            }
        }

        Assertions.assertEquals(deckOfCards.getCardHand().checkFlush(), flush);
    }
}
