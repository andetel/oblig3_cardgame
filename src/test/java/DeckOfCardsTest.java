import idatx2001.oblig3.cardgame.DeckOfCards;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

    @Test
    public void checkThatDealHandMethodDealsTheCorrectAmountOfCards() {
        DeckOfCards deckOfCards = new DeckOfCards();

        deckOfCards.dealHand(5);

        Assertions.assertEquals(deckOfCards.getCardHand().getCards().size(), 5);
    }
}
